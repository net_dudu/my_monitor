cmake_minimum_required(VERSION 3.10.2)

#LANGUAGE CXX 是 project 命令的一个参数，用于指定项目所使用的主要编程语言。
#在这里，LANGUAGE CXX 表示该项目的主要编程语言是 C++。
project(test_monitor LANGUAGES CXX)


#这行代码将 ${CMAKE_SOURCE_DIR}/cmake 路径设置为 CMake 模块的搜索路径。
#模块是一些自定义的 CMake 脚本，可以用来提供额外的功能或设置。
#通过将路径添加到 CMAKE_MODULE_PATH 变量中，CMake 将在指定的路径下搜索模块文件，
#并使其可用于项目的构建过程。

#CMAKE_SOURCE_DIR  cmakelists所在路径

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
set(CMAKE_CXX_STANDARD 17)

add_subdirectory(rpc_manager)
add_subdirectory(test_monitor)
add_subdirectory(proto)
add_subdirectory(display_monitor)



