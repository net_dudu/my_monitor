#include "cpu_load_model.h"

namespace monitor
{
    CpuLoadModel::CpuLoadModel(QObject *parent):MonitorInterModel(parent){
        _header << tr("load_1");
        _header << tr("load_3");
        _header << tr("load_15");
    }

    int CpuLoadModel::rowCount(const QModelIndex &parent )const {
        return _monitor_data.size();
    }
    int CpuLoadModel::columnCount(const QModelIndex &parent )const{
        return COLUMN_MAX;
    }  

    QVariant CpuLoadModel::headerData(int section, Qt::Orientation orientation,
                                  int role) const {
        if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
            return _header[section];
        }

        return MonitorInterModel::headerData(section, orientation, role);
    }

    QVariant CpuLoadModel::data(const QModelIndex& index, int role) const {
        if (index.column() < 0 || index.column() >= COLUMN_MAX) {
            return QVariant();
        }

        if (role == Qt::DisplayRole) {
            if (index.row() < _monitor_data.size() && index.column() < COLUMN_MAX)
            return _monitor_data[index.row()][index.column()];
        }
        return QVariant();
    }

    void CpuLoadModel::UpdateMonitorInfo(
        const monitor::proto::MonitorInfo& monitor_info) {
        beginResetModel();
        _monitor_data.clear();

        _monitor_data.push_back(insert_one_cpu_load(monitor_info.cpu_load()));

        endResetModel();

        return;
    } 

    std::vector<QVariant> CpuLoadModel::insert_one_cpu_load(
        const monitor::proto::CpuLoad& cpu_load) {
        std::vector<QVariant> cpu_load_list;
        for (int i = CpuLoad::CPU_AVG1; i < COLUMN_MAX; i++) {
            switch (i) {
            case CpuLoad::CPU_AVG1:
                cpu_load_list.push_back(QVariant(cpu_load.load_avg_1()));
                break;
            case CpuLoad::CPU_AVG2:
                cpu_load_list.push_back(QVariant(cpu_load.load_avg_2()));
                break;
            case CpuLoad::CPU_AVG3:
                cpu_load_list.push_back(QVariant(cpu_load.load_avg_7()));
                break;
            default:
                break;
            }
        
        }
        return cpu_load_list;
    }



    
} // namespace monitor
