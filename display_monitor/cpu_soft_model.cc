#include "cpu_soft_model.h"

namespace monitor
{
    MonitorBaseModel::MonitorBaseModel(QObject *parent)
    : MonitorInterModel(parent)
    {
        //初始化了一个名为header_的字符串列表。每个字符串代表表头的一列。
        _header<<tr("cpu");
        _header << tr("hi");
        _header << tr("timer");
        _header << tr("net_tx");
        _header << tr("net_rx");
        _header << tr("block");
        _header << tr("irq_poll");
        _header << tr("tasklet");
        _header << tr("sched");
        _header << tr("hrtimer");
        _header << tr("rcu");
    }   

    int MonitorBaseModel::rowCount(const QModelIndex &parent)const {
        return _monitor_data.size();
    }

    int MonitorBaseModel::columnCount(const QModelIndex &parent)const{
        return COLUMN_MAX;
    }

    QVariant MonitorBaseModel::headerData(int section,Qt::Orientation orientation ,int role)const {
        if(role==Qt::DisplayRole&&orientation==Qt::Horizontal){
            return _header[section];
        }
        return MonitorInterModel::headerData(section,orientation,role);
    }

    QVariant MonitorBaseModel::data(const QModelIndex & index,int role)const {

        //返回一个空的QVariant对象。
        //QVariant是Qt框架中的一个类，用于封装各种类型的值，使得这些值可以被作为单一类型来处理。
        //QVariant()表示创建一个空的QVariant对象，它不包含任何值。
        if (index.column() < 0 || index.column() >= COLUMN_MAX) {
            return QVariant();
        }

        ////判断请求的数据角色是否为Qt::DisplayRole，如果是，则表示请求的是显示数据。
        if(role==Qt::DisplayRole){ 
            //且请求的索引在有效的行列范围内，则返回相应的数据
            if (index.row() < _monitor_data.size() && index.column() < COLUMN_MAX){
                return _monitor_data[index.row()][index.column()];
            }
            
        }
        return QVariant();
    }
    

    void MonitorBaseModel::UpdateMonitorInfo(const monitor::proto::MonitorInfo & monitor_info){
        //这些函数用于通知视图（如QTableView）模型已经被重置，并要求视图刷新自己以反映模型的更改。
        beginResetModel();
        _monitor_data.clear();

        for(int i=0;i<monitor_info.soft_irq_size();++i){
            _monitor_data.push_back(insert_one_soft_irq(monitor_info.soft_irq(i)));
        }

        endResetModel();
        return;
    }

    std::vector<QVariant> MonitorBaseModel::insert_one_soft_irq(
        const monitor::proto::SoftIrq& soft_irq){
            std::vector<QVariant> soft_irq_list;
        for(int i=SoftInfo::CPU_NAME; i< COLUMN_MAX;++i){
            switch (i)
            {
                case SoftInfo::CPU_NAME:
                    soft_irq_list.push_back(
                        QVariant(QString::fromStdString(soft_irq.cpu())));
                    break;
                case SoftInfo::HI:
                    soft_irq_list.push_back(QVariant(soft_irq.h1()));
                    break;
                case SoftInfo::TIMER:
                    soft_irq_list.push_back(QVariant(soft_irq.timer()));
                    break;
                case SoftInfo::NET_TX:
                    soft_irq_list.push_back(QVariant(soft_irq.net_tx()));
                    break;
                case SoftInfo::NET_RX:
                    soft_irq_list.push_back(QVariant(soft_irq.net_rx()));
                    break;
                case SoftInfo::BLOCK:
                    soft_irq_list.push_back(QVariant(soft_irq.block()));
                    break;
                case SoftInfo::IRQ_POLL:
                    soft_irq_list.push_back(QVariant(soft_irq.irq_poll()));
                    break;
                case SoftInfo::TASKLET:
                    soft_irq_list.push_back(QVariant(soft_irq.tasklet()));
                    break;
                case SoftInfo::SCHED:
                    soft_irq_list.push_back(QVariant(soft_irq.sched()));
                    break;
                case SoftInfo::HRTIMER:
                    soft_irq_list.push_back(QVariant(soft_irq.hrtimer()));
                    break;
                case SoftInfo::RCU:
                    soft_irq_list.push_back(QVariant(soft_irq.rcu()));
                    break;
                default:
                    break;
            }
         

        }
        return soft_irq_list;

}



} // namespace monitor










