#include "cpu_stat_model.h"

namespace monitor
{
//画表头
CpuStatModel::CpuStatModel(QObject* parent) : MonitorInterModel(parent) {
  _header << tr("name");
  _header << tr("cpu_percent");
  _header << tr("user");
  _header << tr("system");
}

//横行
int CpuStatModel::rowCount(const QModelIndex& parent) const {
  return _monitor_data.size();
}

//列数
int CpuStatModel::columnCount(const QModelIndex& parent) const {
  return COLUMN_MAX;
}

QVariant CpuStatModel::headerData(int section, Qt::Orientation orientation,
                                  int role) const {
  if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
    return _header[section];
  }

  return MonitorInterModel::headerData(section, orientation, role);
}

QVariant CpuStatModel::data(const QModelIndex& index, int role) const {
  if (index.column() < 0 || index.column() >= COLUMN_MAX) {
    return QVariant();
  }

  if (role == Qt::DisplayRole) {
    if (index.row() < _monitor_data.size() && index.column() < COLUMN_MAX)
      return _monitor_data[index.row()][index.column()];
  }
  return QVariant();
}

//更新格点
void CpuStatModel::UpdateMonitorInfo(
    const monitor::proto::MonitorInfo& monitor_info) {
  beginResetModel();
  _monitor_data.clear();

  for (int i = 0; i < monitor_info.cpu_stat_size(); i++) {
    // std::cout <<monitor_info.cpu_stat(i).cpu_name()<<std::endl;
    _monitor_data.push_back(insert_one_cpu_stat(monitor_info.cpu_stat(i)));
  }
  // QModelIndex leftTop = createIndex(0, 0);
  // QModelIndex rightBottom = createIndex(monitor_data_.size(), COLUMN_MAX);
  // emit dataChanged(leftTop, rightBottom, {});

  endResetModel();

  return;
}

//插入一个cpu状态
std::vector<QVariant> CpuStatModel::insert_one_cpu_stat(
    const monitor::proto::CpuStat& cpu_stat) {
  std::vector<QVariant> cpu_stat_list;
  for (int i = CpuStat::CPU_NAME; i < COLUMN_MAX; i++) {
    switch (i) {
      case CpuStat::CPU_NAME:
        cpu_stat_list.push_back(QString::fromStdString(cpu_stat.cpu_name()));
        break;
      case CpuStat::CPU_PERCENT:
        cpu_stat_list.push_back(QVariant(cpu_stat.cpu_percent()));
        break;
      case CpuStat::CPU_USER_PERCENT:
        cpu_stat_list.push_back(QVariant(cpu_stat.user_percent()));
        break;
      case CpuStat::CPU_SYSTEM_PERCENT:
        cpu_stat_list.push_back(QVariant(cpu_stat.sys_percent()));
        break;
      default:
        break;
    }
  }
  return cpu_stat_list;
}






















    

} // namespace monitor




