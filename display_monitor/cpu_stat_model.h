#pragma once

#include <QAbstractTableModel>
#include <vector>
#include "monitor_inter.h"

#include "monitor_info.grpc.pb.h"
#include "monitor_info.pb.h"

namespace monitor
{
    //监控基本模型
    class CpuStatModel : public MonitorInterModel {
        Q_OBJECT

    public:
    explicit CpuStatModel(QObject *parent = nullptr);

    virtual ~CpuStatModel() {}

    int rowCount(const QModelIndex &parent = QModelIndex())const override; 
    int columnCount(const QModelIndex &parent = QModelIndex())const override;  

    QVariant data(const QModelIndex &index,int role = Qt::DisplayRole)const override;
    QVariant headerData(int section,Qt::Orientation orientation ,int role)const override;

    void UpdateMonitorInfo(const monitor::proto::MonitorInfo &monitor_info);

//signals: 是C++中用于声明信号的关键字。在Qt框架中，信号（signals）用于在对象之间进行通信。
//当特定事件发生时，一个对象可以通过发射（emit）信号来通知其他对象。其他对象可以连接（connect）
//到该信号，并在接收到信号时执行相应的槽函数（slot）。
signals:
    void dataChanged(const QModelIndex  &topLeft,const QModelIndex & bottomRight,
                    const QVector<int> &roles);
private:
    std::vector<QVariant> insert_one_cpu_stat(const monitor::proto::CpuStat &cpu_stat);

private:
    std::vector<std::vector<QVariant>> _monitor_data;
    QStringList _header;

    enum CpuStat{
        CPU_NAME = 0,
        CPU_PERCENT,
        CPU_USER_PERCENT,
        CPU_SYSTEM_PERCENT,
        COLUMN_MAX
    };

    };
   
} // namespace monitor










