#include <QColor>
#include <QFont> //描述字体的类


#include "monitor_inter.h"

namespace monitor
{
  

QVariant MonitorInterModel::headerData(int section, Qt::Orientation orientation,
                                       int role) const {

    if (role == Qt::FontRole) {  // 设定字体
    return QVariant::fromValue(QFont("Microsoft YaHei", 10, QFont::Bold));
  }

  if (role == Qt::BackgroundRole) {  //背景色
    return QVariant::fromValue(QColor(Qt::lightGray));
  }

  // 返回使用父类的headerData函数处理得到的结果
  return QAbstractTableModel::headerData(section, orientation, role);
}

QVariant MonitorInterModel::data(const QModelIndex &index, int role)const {
    if (role == Qt::TextAlignmentRole) {
    return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
  }

  if (role == Qt::TextColorRole) {
    return QVariant::fromValue(QColor(Qt::black));
  }

  if (role == Qt::BackgroundRole) {
    return QVariant::fromValue(QColor(Qt::white));
  }

  return QVariant();
}


} // namespace monitor
