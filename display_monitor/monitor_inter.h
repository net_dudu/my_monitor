#pragma once
#include <QAbstractTableModel>
#include <QObject>

namespace monitor{

class MonitorInterModel:public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit MonitorInterModel(QObject *parent = nullptr)
    : QAbstractTableModel(parent){}

    virtual ~MonitorInterModel() {}

    //常量成员函数
    QVariant data(const QModelIndex &index,int role=Qt::DisplayRole) const override;

    QVariant headerData(int section,Qt::Orientation orientation,int role)const override;

};


}


//在 C++/Qt 中，Q_OBJECT 宏是用于声明包含 Qt 特殊功能的类的一种特殊方式。它为类添加了一些在 Qt 元对象系统（Meta-Object System）中需要的特性，包括信号和槽（signals and slots）、元对象信息（meta-object information）和动态属性（dynamic properties）等。