#include <QStandardItemModel>
#include <QtWidgets>
#include <thread>
#include <string>

#include "cpu_load_model.h"
#include "cpu_soft_model.h"
#include "cpu_stat_model.h"
#include "mem_model.h"
#include "net_model.h"

#include "monitor_info.grpc.pb.h"
#include "monitor_info.pb.h"

namespace monitor
{
    class Monitorwidget:public QWidget
    {
        Q_OBJECT
    public:
        explicit Monitorwidget(QWidget * parent = nullptr);
        ~Monitorwidget(){}

// 这些函数是用于初始化不同类型监视器小部件（QWidget）的函数。它们的功能如下：
// QWidget* ShowAllMonitorWidget(const std::string& name)：创建一个显示所有监视器的小部件，并返回该小部件的指针。
// QWidget* InitCpuMonitorWidget()：创建一个CPU监视器的小部件，并返回该小部件的指针。
// QWidget* InitSoftIrqMonitorWidget()：创建一个软中断监视器的小部件，并返回该小部件的指针。
// QWidget* InitMemMonitorWidget()：创建一个内存监视器的小部件，并返回该小部件的指针。
// QWidget* InitNetMonitorWidget()：创建一个网络监视器的小部件，并返回该小部件的指针。
// QWidget* InitButtonMenu(const std::string& name)：创建一个带有按钮菜单的小部件，并返回该小部件的指针。

        QWidget* ShowAllMonitorWidget(const std::string& name);
        QWidget* InitCpuMonitorWidget();
        QWidget* InitSoftIrqMonitorWidget();
        QWidget* InitMemMonitorWidget();
        QWidget* InitNetMonitorWidget();
        QWidget* InitButtonMenu(const std::string& name);

        void UpdateData(const monitor::proto::MonitorInfo& monitor_info);

//Qt 框架中的槽（slot）函数。槽函数是用于响应用户界面操作或特定信号的函数。在这种情况下，这些槽函数可能与用户界面中的按钮点击事件相关联。
    private slots:
        void ClickCpuButton();
        void ClickSoftIrqButton();
        void ClickMemButton();
        void ClickNetButton();  

    private:
        QTableView* monitor_view_ = nullptr; //用于显示监视器数据的表格视图
        QTableView* cpu_load_monitor_view_ = nullptr;
        QTableView* cpu_stat_monitor_view_ = nullptr;
        QTableView* mem_monitor_view_ = nullptr;
        QTableView* net_monitor_view_ = nullptr;

        MonitorBaseModel* monitor_model_ = nullptr;
        CpuLoadModel* cpu_load_model_ = nullptr;
        CpuStatModel* cpu_stat_model_ = nullptr;
        MemModel* mem_model_ = nullptr;
        NetModel* net_model_ = nullptr;

        QStackedLayout* stack_menu_ = nullptr; //用于管理按钮菜单的堆叠布局


    };


    
} // namespace monitor









