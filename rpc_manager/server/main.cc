#include <iostream>
#include <grpc/grpc.h>

#include <grpcpp/server_builder.h>
#include "rpc_manager.h"

//编译时求值的常量表达式（Constant expression）。运行时候不能被修改
constexpr char kServerPortInfo[] ="0.0.0.0:50051";

void InitServer(){

    //创建一个 grpc::ServerBuilder 对象 builder，该对象用于构建 gRPC 服务器。
    grpc::ServerBuilder builder;
    // builder.AddListeningPort() 方法，为服务器添加一个监听端口。
    builder.AddListeningPort(kServerPortInfo,grpc::InsecureServerCredentials());
    // gRPC 服务的具体实现类
    monitor::GrpcManagerImpl grpc_server;
    builder.RegisterService(&grpc_server);

    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());

    //server->Wait() 方法，等待服务器启动并开始接收客户端请求
    server->Wait();
    return;

}

int main(){
    InitServer();
    return 0;

}














