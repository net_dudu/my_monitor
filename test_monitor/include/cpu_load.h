#pragma once

#include <string>
#include "monitor_inter.h"
#include "monitor_info.grpc.pb.h"
#include "monitor_info.pb.h"

namespace monitor{

class CpuLoadMonitor :public MonitorInter
{
public:
    CpuLoadMonitor() {}
    ~CpuLoadMonitor() {}

    void UpdateOnce(monitor::proto::MonitorInfo* monitor_info);
    void Stop() override{}

private:
    float _load_avg_1;
    float _load_avg_2;
    float _load_avg_3;

};

}

