#pragma once
#include <string>
#include <unordered_map>

#include <boost/chrono.hpp>

#include "monitor_inter.h"
#include "monitor_info.grpc.pb.h"
#include "monitor_info.pb.h"


namespace monitor
{
    struct cpu_soft{
        std::string cpu_name;
        int64_t hi;
        int64_t timer;
        int64_t net_tx;
        int64_t net_rx;
        int64_t block;
        int64_t irq_poll;
        int64_t tasklet;
        int64_t sched;
        int64_t hrtimer;
        int64_t rcu;
        boost::chrono::steady_clock::time_point timepoint;
    };

    class CpuSoftIrqMonitor:public MonitorInter{


        
    public:
        CpuSoftIrqMonitor(){}
        void UpdateOnce(monitor::proto::MonitorInfo* monitor_info);

        void Stop() override{}

    private:

        std::unordered_map<std::string,cpu_soft> _cpu_softirqs;



    };
    
} // namespace monitor





