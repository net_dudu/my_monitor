#pragma once 

#include <string>

#include <unordered_map>

#include "monitor_inter.h"

#include "monitor_info.grpc.pb.h"
#include "monitor_info.pb.h"


namespace monitor
{
    struct cpu_info{
        std::string cpu_name;
        float user;
        float system;
        float idle;
        float nice;
        float io_wait;
        float irq;
        float soft_irq;
        float steal;
        float guest;
        float guest_nice;
    };

    class CpuStatMonitor:public MonitorInter{


        
    public:
        CpuStatMonitor(){}
        void UpdateOnce(monitor::proto::MonitorInfo* monitor_info);

        void Stop() override{}

    private:

        std::unordered_map<std::string,cpu_info> _cpu_stat_map;

    };
    
} // namespace monitor











