#pragma once 

#include <string>

#include <unordered_map>

#include "monitor_inter.h"

#include "monitor_info.grpc.pb.h"
#include "monitor_info.pb.h"


namespace monitor
{
    struct Mem_info{
        std::string cpu_name;
        int64_t total;
        int64_t free;
        int64_t avail;
        int64_t buffers;
        int64_t cached;
        int64_t swap_cached;
        int64_t active;
        int64_t in_active;
        int64_t active_anon;
        int64_t inactive_anon;
        int64_t active_file;
        int64_t inactive_file;
        int64_t dirty;
        int64_t writeback;
        int64_t anon_pages;
        int64_t mapped;
        int64_t kReclaimable;
        int64_t sReclaimable;
        int64_t sUnreclaim;
    };

    class MemMonitor:public MonitorInter{      
    public:
        MemMonitor(){}

        void UpdateOnce(monitor::proto::MonitorInfo* monitor_info);

        void Stop() override{}

    // private:
    //     std::unordered_map<std::string,Mem_info> _cpu_stat_map;

    };
    
} // namespace monitor
