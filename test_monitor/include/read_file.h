#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <sstream>


namespace monitor{
    class ReadFile{
        public:
    
        explicit ReadFile(std::string name)
        :ifs_(name),
        fileName(name)
        {}

        ~ReadFile(){ifs_.close();}

        bool get_lines(std::vector<std::string> &args);

        //不需要访问或修改 ReadFile 类的实例成员,只是简单地
        //从指定文件中读取一些文本行，并返回一个向量
        static std::vector<std::string> File2Vector(
            const std::string fileName,const int line_number){

            std::ifstream ifs(fileName);
            std::vector<std::string> FileVector;
            if(!ifs.is_open()){
                std::cout <<"failed to open the file " <<std::endl;
            }
            for(int i=0;i<line_number;++i){
                std::string line;
                std::getline(ifs,line);
                if(line.empty()){
                    break;
                }
                FileVector.push_back(line);
            }



            if(ifs.bad()){
                std::cout << "error Reading file ..." <<std::endl; 
            }

            ifs.close();
            return FileVector;
        }

        // void test(std::string filename){
        //     std::vector<std::string> lines;
        //     std::ifstream file(filename);
        //     if (file.is_open()) {
        //         std::string line;
        //         while (std::getline(file, line)) {
        //             lines.push_back(line);
        //         }
        //         file.close();
        //     } else {
        //         std::cerr << "Error opening file: " << filename << std::endl;
        //     }
        //     for (const auto& line : lines) {
        //         std::cout << line << std::endl;
        //     }
        // }


        private:
            std::ifstream ifs_;
            std::string fileName;
    };
}

// void test(){
//     std::string filename="utils.h";
//     monitor::ReadFile myfile(filename);
//     std::vector<std::string> res=myfile.File2Vector(filename);
//     for(int i=0;i<res.size();i++){
//         std::cout << res[i] <<std::endl;
//     }


// }

// int main(){
//     test();
// }


