#pragma once
#include <boost/chrono.hpp>
namespace monitor{
    class Utils{
        public:
            static double between_2_TimeSecond(
                const boost::chrono::steady_clock::time_point &t1,
                const boost::chrono::steady_clock::time_point &t2
            ){
                boost::chrono::duration<double> t12=t1-t2;
                return t12.count();
            }
    };
}
