#include "cpu_load.h"

#include "read_file.h"

#include "monitor_info.grpc.pb.h"
#include "monitor_info.pb.h"


namespace monitor {

void CpuLoadMonitor::UpdateOnce(monitor::proto::MonitorInfo* monitor_info){
    //读文件 （未）
    ReadFile cpu_load_file(std::string("/proc/loadavg"));
    std::vector<std::string> cpu_load;
    cpu_load_file.get_lines(cpu_load);

    //cpu_load_file.test(std::string("/proc/loadavg"));
    //std::cout <<"cpu_load .size :" <<cpu_load.size()<<std::endl; 
    // for (const auto& load : cpu_load) {
    //     std::cout << load << std::endl;
    // }

    _load_avg_1=std::stof(cpu_load[0]);
    _load_avg_2=std::stof(cpu_load[1]);
    _load_avg_3=std::stof(cpu_load[2]); 

    //mutable_cpu_load() 是一个成员函数调用，它是由 
    //Protocol Buffers 自动生成的用于修改或访问 cpu_load 字段的方法。
    auto cpu_load_msg=monitor_info->mutable_cpu_load();

    cpu_load_msg->set_load_avg_1(_load_avg_1);
    cpu_load_msg->set_load_avg_2(_load_avg_2);
    cpu_load_msg->set_load_avg_7(_load_avg_3);

    return;
}


}


