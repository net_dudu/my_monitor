#include "mem_stat.h"
#include "read_file.h"

namespace monitor
{
    static constexpr float KB2GB=1000*1000;
    void MemMonitor::UpdateOnce(monitor::proto::MonitorInfo* monitor_info){

    ReadFile mem_file("/proc/meminfo");
    Mem_info mem_info;

    std::vector<std::string> mem_datas; //一行数据，进行分解

    while(mem_file.get_lines(mem_datas)){
        if (mem_datas[0] == "MemTotal:") {
        mem_info.total = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "MemFree:") {
        mem_info.free = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "MemAvailable:") {
        mem_info.avail = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Buffers:") {
        mem_info.buffers = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Cached:") {
        mem_info.cached = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "SwapCached:") {
        mem_info.swap_cached = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Active:") {
        mem_info.active = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Inactive:") {
        mem_info.in_active = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Active(anon):") {
        mem_info.active_anon = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Inactive(anon):") {
        mem_info.inactive_anon = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Active(file):") {
        mem_info.active_file = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Inactive(file):") {
        mem_info.inactive_file = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Dirty:") {
        mem_info.dirty = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Writeback:") {
        mem_info.writeback = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "AnonPages:") {
        mem_info.anon_pages = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "Mapped:") {
        mem_info.mapped = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "KReclaimable:") {
        mem_info.kReclaimable = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "SReclaimable:") {
        mem_info.sReclaimable = std::stoll(mem_datas[1]);
        } else if (mem_datas[0] == "SUnreclaim:") {
        mem_info.sUnreclaim = std::stoll(mem_datas[1]);
        }
        mem_datas.clear();
    }// 值到结构体里了

    //用于获取对 mem_info 字段的可变引用,只有一个对象
    auto mem_detail = monitor_info->mutable_mem_info();
    //设置字符值并转为GB

    mem_detail->set_memtotal(mem_info.total / KB2GB);
    mem_detail->set_memfree(mem_info.free / KB2GB);
    mem_detail->set_memavailable(mem_info.avail / KB2GB);
    mem_detail->set_buffers(mem_info.buffers / KB2GB);
    mem_detail->set_cached(mem_info.cached / KB2GB);
    mem_detail->set_swapcached(mem_info.swap_cached / KB2GB);
    mem_detail->set_active(mem_info.active / KB2GB);
    mem_detail->set_inactive(mem_info.in_active / KB2GB);
    mem_detail->set_active_anon(mem_info.active_anon / KB2GB);
    mem_detail->set_inactive_anon(mem_info.inactive_anon / KB2GB);
    mem_detail->set_active_file(mem_info.active_file / KB2GB);
    mem_detail->set_inactive_file(mem_info.inactive_file / KB2GB);
    mem_detail->set_dirty(mem_info.dirty / KB2GB);
    mem_detail->set_writeback(mem_info.writeback / KB2GB);
    mem_detail->set_anonpages(mem_info.anon_pages / KB2GB);
    mem_detail->set_mapped(mem_info.mapped / KB2GB);
    mem_detail->set_kreclaimable(mem_info.kReclaimable / KB2GB);
    mem_detail->set_sreclaimable(mem_info.sReclaimable / KB2GB);
    mem_detail->set_sunreclaim(mem_info.sUnreclaim / KB2GB);

    return;
}
    
} // namespace name























