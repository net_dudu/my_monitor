#include "net_stat.h"
#include "read_file.h"
#include "utils.h"

namespace monitor {

    void NetMonitor::UpdateOnce(monitor::proto::MonitorInfo* monitor_info){
        ReadFile net_file(std::string("/proc/net/dev"));
        std::vector<std::string> line;
        while (net_file.get_lines(line))
        {
            std::string name=line[0];

            if(name.find(':')==name.size()-1 && line.size()>=10){
                Net_info net_info;
                //如果字符串 name 的最后一个字符是冒号 ':'，
                //则会执行 name.pop_back() 来移除该字符。
                name.pop_back();

                //stoll 字符串转换为长长整型（long long int）
                net_info.name=name;
                net_info.rcv_bytes = std::stoll(line[1]);
                net_info.rcv_packets = std::stoll(line[2]);
                net_info.err_in = std::stoll(line[3]);
                net_info.drop_in = std::stoll(line[4]);
                net_info.snd_bytes = std::stoll(line[9]);
                net_info.snd_packets = std::stoll(line[10]);
                net_info.err_out = std::stoll(line[11]);
                net_info.drop_out = std::stoll(line[12]);
                net_info.timepoint = boost::chrono::steady_clock::now();

                auto iter = _net_info_map.find(name);

                if(iter !=_net_info_map.end()){
                    //将 iter->second 的值移动到 old 变量中
                    //移动操作会将原来的对象置为无效状态，并从源位置转移资源所有权到目标位置，以提高性能。
                    // 怎么感受到 ，它是提升性能的？
                    Net_info old=std::move(iter->second);
                    double period =Utils::between_2_TimeSecond(net_info.timepoint,old.timepoint);
                    //add_net_info()用于向 MonitorInfo 消息中的 net_info 字段添加一个新元素并返回对该新元素的引用。
                    auto one_net_msg=monitor_info->add_net_info();

                    one_net_msg->set_name(net_info.name);
                    one_net_msg->set_sen_rate((net_info.snd_bytes-old.snd_bytes)/1024/period);
                    one_net_msg->set_rcv_rate((net_info.rcv_bytes-old.rcv_bytes)/1024/period);
                    one_net_msg->set_sen_packets_rate((net_info.snd_packets-old.snd_packets)/1024/period);
                    one_net_msg->set_rcv_packets_rate((net_info.rcv_packets-old.rcv_packets)/1024/period);

                }
                _net_info_map[name]=net_info;

            }
            //使字符串变为空字符串，即将其长度设置为0，丢弃所有字符
            line.clear();
        }
        return;
        
    }



}//namespace monitor






