#include "read_file.h"

//尖括号<>包含的头文件是一般来说，是库文件对应的头文件，而使用双引号
//包含的则是我们自己的编写的头文件。

//库文件 的头文件大致分为三类
//c/c++标准库对应的一些头文件、操作系统的系统头文件、
//根据系统安装的一些他特定功能库的对应的头文件

//双引号形式会查找当前目录，尖括号形式则不会
//以linux 系统为例子，具体查找顺序和路径如下：
//1 当前（仅双引号形式）
//2 编译时 指定的头文件目录（由gcc -l参数指定）
//3 系统环境变量 CPLUS_INCLUDE_PATH（c++头文件）或 C_INCLUDE_PATH（c头文件）指定的目录
//4 gcc 默认目录 /usr/include;/usr/local/include;/usr/lib/gcc/x86_64-linux-gnu/5/include等
// (注：最后一个路径是gcc程序的库文件地址，各个用户的系统上可能不一样)

//如果各个目录下存在相同的文件，找到哪个就使用哪个，这是顺序非常重要

namespace monitor {

bool ReadFile::get_lines(std::vector<std::string> &vec){
    // std::string line;
    // std::fstream tmp_ifs(fileName);
    // std::cout << "fileName : " <<fileName <<std::endl;

    // if (tmp_ifs.is_open()) {
    //     std::getline(tmp_ifs, line);
    //     //lines.push_back(line);
    //     std::cout << "if (tmp_ifs.is_open()) : " <<line<<std::endl;

    // } else {
    //     std::cerr << "Error opening file: " << fileName << std::endl;
    // }


    // std::getline(tmp_ifs,line);
    // std::cout << " ReadFile line :" <<line <<std::endl;

    // //eof 检测文件是否达到末尾
    // if(tmp_ifs.eof()||line.empty()){
    //     std::cout << "ifs_.eof()||line.empty() ... " <<std::endl;
    //     return false;
    // }

    // std::istringstream ss(line);
    // while(!ss.eof()){
    //     std::string word;
    //     ss>>word;
    //     vec.push_back(word);
    // }
    // return true;
    std::string line;
    std::getline(ifs_, line);
    if (ifs_.eof() || line.empty()) {
        return false;
    }

    std::istringstream line_ss(line);
    while (!line_ss.eof()) {
        std::string word;
        line_ss >> word;
        vec.push_back(word);
    }
    return true;
}

//.eof() 是 std::istream 类的成员函数，用于检查输入流是否已经到达文件末尾




    
}






